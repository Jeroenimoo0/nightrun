package com.frostedberry.onegameamonth.onegam;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

import com.frostedberry.onegameamonth.core.Core;
import com.frostedberry.onegameamonth.core.audio.AudioPlayer;
import com.frostedberry.onegameamonth.core.data.Database;
import com.frostedberry.onegameamonth.core.data.Property;
import com.frostedberry.onegameamonth.core.data.PropertyCollection;
import com.frostedberry.onegameamonth.core.gui.GuiLoadScreenTemplate;
import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.text.TextLoader;
import com.frostedberry.onegameamonth.core.tick.KeyEvent;
import com.frostedberry.onegameamonth.core.tick.Tick;
import com.frostedberry.onegameamonth.onegam.gui.GuiLoadScreen;
import com.frostedberry.onegameamonth.onegamres.Res;

import static org.lwjgl.opengl.GL11.*;

public class Main extends Core {
	/**
	 * @author Jeroen van de Haterd aka. Jeroenimoo0
	 * Copyright Frosted berry.
	 */
	private static final long serialVersionUID = 1L;
	
	public Quality quality = Quality.High;
	public Difficulty difficulty = Difficulty.Easy;
	
	@Override
	protected void render() {
		if(pc.isLoaded()) {
			if(TextLoader.isLoaded()) {
				if(isShowFps()) {	
					Render.renderString("" + getFps(), 1, 0, 8, 0.8f, 0.8f, 0.8f);
				}
			}
		}
	}

	@Override
	protected void tick(Tick tick) {
		for(KeyEvent ke : tick.getKeyEvents()) {
			if(ke.key == Keyboard.KEY_END && ke.keyDown) {
				pc.writeToDatabase();
				Database.saveFile();
			}
			if(ke.key == Keyboard.KEY_P && ke.keyDown) {
				if(Boolean.parseBoolean(pc.getProperty("showFps"))) setShowFps(false);
				else setShowFps(true);
				saveData();
			}
			if(ke.key == Keyboard.KEY_F11 && ke.keyDown) {
				if(!isFullscreen()) enableFullScreen();
				else disableFullScreen();
			}
			if(ke.key == Keyboard.KEY_B && ke.keyDown) {
				AudioPlayer.playAsMusic("soundtrack", 1, 1, true);
			}
			if(ke.key == Keyboard.KEY_V && ke.keyDown) {
				System.out.println(AudioPlayer.getAudioVolume("soundtrack"));
			}
		}
	}
	
	@Override
	protected void initBeforeGl() {
		setPrintFps(false);
	}
	
	@Override
	protected void initGl() {	
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, getGameWidth(), getGameHeight(), 0, 1, -1);
        glMatrixMode(GL_MODELVIEW);
        
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
        /*
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glEnable(GL_ALPHA);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glAlphaFunc(GL_GREATER, 0.5f);
        glEnable(GL_ALPHA_TEST);
        
        //glEnable(GL_DEPTH_TEST);
        //glDepthMask(true);
        //glDepthFunc(GL_LEQUAL);
        //glDepthRange(0.0f, 1.0f);
        
		/*
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
        glEnable(GL_DEPTH_TEST);
        */
	}

	@Override
	protected void initAfterGl() {
	}

	@Override
	public void start() {
		
	}

	@Override
	public void stop() {
		
	}
	
	@Override
	public int getGameWidth() {
		return 320;
	}
	
	@Override
	public int getGameHeight() {
		return 180;
	}
	
	@Override
	public Object getResObject() {
		return new Res();
	}
	
	@Override
	public boolean isDebug() {
		return !isInBrowser();
	}

	@Override
	public int getWindowWidth() {
		if(!isInBrowser()) return 960;
		else return Display.getWidth();
	}

	@Override
	public int getWindowHeight() {
		if(!isInBrowser()) return 540;
		else return Display.getWidth();
	}

	@Override
	protected GuiLoadScreenTemplate getLoadScreen() {
		return new GuiLoadScreen(this);
	}

	PropertyCollection pc = new PropertyCollection() {
		
		@Override
		public void init() {
			add(new Property("showFps", "false"));
			add(new Property("difficulty", "0"));
			add(new Property("quality", "2"));
			add(new Property("volume", "1.0"));
			
			add(new Property("highscore_endless_hard", "0"));
			add(new Property("highscore_endless_normal", "0"));
			add(new Property("highscore_endless_easy", "0"));
		}
	};
	
	@Override
	public PropertyCollection getPropertyCollection() {
		return pc;
	}

	@Override
	public void onPropertiesLoaded() {
		setShowFps(Boolean.parseBoolean(pc.getProperty("showFps")));
		quality = Quality.fromString(pc.getProperty("quality"));
		difficulty = Difficulty.fromString(pc.getProperty("difficulty"));
		AudioPlayer.setVolume(Float.parseFloat(pc.getProperty("volume")));
	}
	
	@Override
	public void setShowFps(boolean show) {
		pc.setProperty("showFps", show);
		super.setShowFps(show);
	}

	@Override
	public String getGameName() {
		return "Night run";
	}
	
	public void setQuality(Quality quality) {
		this.quality = quality;
		pc.setProperty("quality", quality.toInt());
	}
	
	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
		pc.setProperty("difficulty", difficulty.toInt());
	}
	
	public void setVolume(float volume) {
		AudioPlayer.setVolume(volume);
		pc.setProperty("volume", volume);
	}
	
	public void updateHighScore(String name, int score) {
		pc.setProperty("highscore_" + name + "_" + difficulty.toString().toLowerCase(), score);
	}
	
	public int getHighScore(String name) {
		return Integer.parseInt(pc.getProperty("highscore_" + name + "_" + difficulty.toString().toLowerCase()));
	}
	
	public void resetHighScores() {
		for(Property p : pc.getProperties().values()) {
			if(p.getName().startsWith("highscore")) p.setToDefault();
		}
	}
	
	public void saveData() {
		pc.writeToDatabase();
		Database.saveFile();
	}
}
