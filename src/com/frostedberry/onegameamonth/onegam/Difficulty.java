package com.frostedberry.onegameamonth.onegam;

public enum Difficulty {
	Easy(0), Normal(1) ,Hard(2) ,GODLIKE(3);
	
	int difficultyNumber;
	
	private Difficulty(int difficultyNumber) {
		this.difficultyNumber = difficultyNumber;
	}
	
	public int toInt() {
		return difficultyNumber;
	}
	
	public static Difficulty fromInt(int i) {
		if(i == 0) return Easy;
		else if(i == 1) return Normal;
		else if(i == 2) return Hard;
		else if(i == 3) return GODLIKE;
		else return null;
	}
	
	public static Difficulty fromString(String s) {
		if(s.equalsIgnoreCase("0")) return Easy;
		else if(s.equalsIgnoreCase("1")) return Normal;
		else if(s.equalsIgnoreCase("2")) return Hard;
		else if(s.equalsIgnoreCase("3")) return GODLIKE;
		else return null;
	}
}
