package com.frostedberry.onegameamonth.onegam;

public enum Quality {
	Low(0), Medium(1) ,High(2);
	
	int qualityNumber;
	
	private Quality(int qualityNumber) {
		this.qualityNumber = qualityNumber;
	}
	
	public int toInt() {
		return qualityNumber;
	}
	
	public static Quality fromInt(int i) {
		if(i == 0) return Low;
		else if(i == 1) return Medium;
		else if(i == 2) return High;
		else return null;
	}
	
	public static Quality fromString(String s) {
		if(s.equalsIgnoreCase("0")) return Low;
		else if(s.equalsIgnoreCase("1")) return Medium;
		else if(s.equalsIgnoreCase("2")) return High;
		else return null;
	}
}
