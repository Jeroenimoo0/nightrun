package com.frostedberry.onegameamonth.onegam.world;

import java.awt.Rectangle;

import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.tick.Tick;

public class Wall extends WorldObject implements Intersectable {
	protected float x;
	protected float height;
	protected float r = 1;
	protected float g = 1;
	protected float b = 1;
	protected boolean passedPlayer;
	protected boolean changeHeight;
	protected float yo;
	
	public Wall(World world) {
		super(world);
		r = 0;
		g = 0;
		b = 0;
	}
	
	public Wall(World world, float x, float height) {
		super(world);
		this.x = x;
		this.height = height;
		r = 0;
		g = 0;
		b = 0;
	}
	
	@Override
	public void render() {
		Render.renderColor(x, 128 - height + yo, 4, 12, r, g, b);
		if(world.getGame().getMain().difficulty.toInt() == 1 && changeHeight) {
			if(height == 0) Render.renderTexturePixel("speedicon_0", x, 128 - height + yo, 8, 8, 1, 1, 1);
			else Render.renderTexturePixel("speedicon_2", x, 128 - height + yo, 8, 8, 1, 1, 1);
		}
	}
	
	@Override
	public void tick(Tick tick) {
		for(Player player : world.getPlayers()) {
			if(x < 60 && !passedPlayer) {
				passedPlayer = true;
				world.getGame().onIntersectablePass(player, this);
			}
		}
		
		if(changeHeight && x < 250) {
			if(height == 0) {
				if(yo > -25) yo--;
			}
			else if(yo < 25) yo++;
		}
	}

	@Override
	public boolean intersects(Intersectable i) {
		return i.getRectangle().intersects(getRectangle());
	}

	@Override
	public Rectangle getRectangle() {
		return new Rectangle((int)x - 4, 128 - (int)height + (int)yo - 12, 4 * 2, 12 * 2);
	}
}
