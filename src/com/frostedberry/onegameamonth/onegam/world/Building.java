package com.frostedberry.onegameamonth.onegam.world;

import java.util.ArrayList;
import java.util.List;

import com.frostedberry.onegameamonth.core.common.Random;
import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.tick.Tick;

public class Building extends WorldObject {
	protected float x;
	protected float height;
	protected float width;
	
	protected List<Window> windows;
	protected int maxWindowsWidth;
	protected int maxWindowsHeight;
	
	protected BuildingAttachment ba;
	
	public Building(World world) {
		super(world);
		windows = new ArrayList<>();
		this.world = world;
		width = 6 * (Random.nextInt(6) + 5);
		height = 6 * (Random.nextInt(6) + 5);
		maxWindowsWidth = (int) (width / 6);
		maxWindowsHeight = (int) (height / 6);
		
		for(int cx = 0; cx < maxWindowsWidth; cx++) {
			for(int cy = 0; cy < maxWindowsHeight; cy++) {
				if(Random.nextInt(6) <= 0) {
					windows.add(new Window(cx, cy));
				}
			}
		}
		
		if(Random.nextInt(3) == 0) {
			if(Random.nextInt(2) == 0) {
				ba = new Antenne(Random.nextInt((int) (width + 1)), Random.nextInt(3) + 4, Random.nextInt(3) + 8);
			}
			else {
				int offset = Random.nextInt(25) - 12;
				int width = 24;
				if(Math.abs(offset) + width > this.width) width = (int) (this.width - Math.abs(offset));
				ba = new RoofHouse(offset, width, Random.nextInt(5) + 5);
			}
		}
	}
	
	@Override
	public void render() {
		Render.renderColor(x, world.getStandartBuildingHeight() - height, width, height, world.getBuildingColor());
		
		if(ba != null) ba.render(this);
		
		for(Window w : windows) {
			Render.renderTexture("window", (x - width + 6) + 12 * w.x, world.getStandartBuildingHeight() - height * 2 + 6 + 12 * w.y, 8);
		}
	}
	
	public void renderWindows() {
		for(Window w : windows) {
			Render.renderTexture("window", (x - width + 6) + 12 * w.x, world.getStandartBuildingHeight() - height * 2 + 6 + 12 * w.y, 8);
		}
	}
	
	public void renderBuildings() {
		Render.renderColor(x, world.getStandartBuildingHeight() - height, width, height, world.getBuildingColor());
		
		if(ba != null) ba.render(this);
	}
	
	@Override
	public void tick(Tick tick) {
		
	}
	
	public float getX() {
		return x;
	}
	
	public void setX(float x) {
		this.x = x;
	}
	
	public float getWidth() {
		return width;
	}
	
	private class Window {
		protected int x;
		protected int y;
		
		public Window(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}
	
	private abstract class BuildingAttachment {
		abstract void render(Building b);
	}
	
	private class Antenne extends BuildingAttachment {
		protected int x;
		protected int width;
		protected int height;
		
		public Antenne(int x, int width, int height) {
			this.x = x;
			this.width = width;
			this.height = height;
		}
		
		@Override
		void render(Building b) {
			Render.renderColor(b.x - b.width + width + x, world.getStandartBuildingHeight() - b.height * 2 - 1, width, 1, b.world.getBuildingColor());
			Render.renderColor(b.x - b.width + width + x, world.getStandartBuildingHeight() - b.height * 2 - 2, width / 1.5f, 2, b.world.getBuildingColor());
			Render.renderColor(b.x - b.width + width + x, world.getStandartBuildingHeight() - b.height * 2 - height, width / 4.5f, height, b.world.getBuildingColor());
		}
	}
	
	private class RoofHouse extends BuildingAttachment {
		protected int x;
		protected int width;
		protected int height;
		
		public RoofHouse(int x, int width, int height) {
			this.x = x;
			this.width = width;
			this.height = height;
		}
		
		@Override
		void render(Building b) {
			Render.renderColor(b.x + x, world.getStandartBuildingHeight() - b.height * 2 - height, width, height, b.world.getBuildingColor());
		}
	}
}
