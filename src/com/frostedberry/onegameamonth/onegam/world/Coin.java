package com.frostedberry.onegameamonth.onegam.world;

import java.awt.Rectangle;

import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.tick.Tick;

public class Coin extends WorldObject implements Intersectable {
	protected float x;
	protected float y;
	protected float size = 8;
	protected boolean collected;
	protected float visability;
	
	public Coin(World world, float x, float y) {
		super(world);
		this.x = x;
		this.y = y;
		
		init();
	}
	
	public Coin(World world) {
		super(world);
		
		init();
	}
	
	private void init() {
		visability = 1;
	}
	
	@Override
	void render() {
		Render.renderTexturePixel("coin", x, y, size, size, 1, 1, 1, visability);
		Render.renderTexturePixel("speedicon_1", x, y, size * 0.6f, size * 0.6f, 1, 1, 1, visability);
	}
	
	@Override
	void tick(Tick tick) {
		if(!collected) {
			for(Player p : world.getPlayers()) {
				if(p.intersects(this)) {
					if(world.getMoveSpeed() > 1) {
						world.setMoveSpeed(world.getMoveSpeed() - 0.5f);
					}
					collected = true;
				}
			}
		}
		else {
			if(visability > 0) visability -= 0.1f;
		}
	}
	
	@Override
	public Rectangle getRectangle() {
		return new Rectangle((int)(x - size), (int)(y - size), (int)size * 2, (int)size * 2);
	}
	
	@Override
	public boolean intersects(Intersectable i) {
		return getRectangle().intersects(i.getRectangle());
	}
}
