package com.frostedberry.onegameamonth.onegam.world;

import com.frostedberry.onegameamonth.core.common.Random;
import com.frostedberry.onegameamonth.core.common.SineWiggler;
import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.tick.Tick;

public class Star extends WorldObject {
	protected float x;
	protected int height;
	protected float y;
	protected int size;
	protected boolean falling;
	
	private float fallSpeed = 1;
	
	private SineWiggler wiggler;
	
	public Star(World world) {
		super(world);
		init();
	}
	
	public Star(World world, float x) {
		super(world);
		this.x = x;
		init();
	}
	
	private void init() {
		wiggler = new SineWiggler(Random.nextInt(6) + 10, (Random.nextInt(4) + 2d) / 100d);
		falling = true;
		height = 20 + Random.nextInt(41);
		size = 8 + Random.nextInt(5);
	}
	
	@Override
	public void render() {
		Render.renderLine(x, y, x, 0, 2, 0, 0, 0);
		Render.renderTexture("star", x, y, size);
	}
	
	@Override
	public void tick(Tick tick) {
		if(falling) {
			if(y < height) {
				y += fallSpeed;
				fallSpeed += 0.1f;
				if(y > height) y = height;
			}
			else falling = false;
		}
		else {
			y = (float) (height + wiggler.getCurrent());
			wiggler.next();
		}
	}
	
	public void setX(float x) {
		this.x = x;
	}
}
