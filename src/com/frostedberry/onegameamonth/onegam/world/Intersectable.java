package com.frostedberry.onegameamonth.onegam.world;

import java.awt.Rectangle;

public interface Intersectable {
	public boolean intersects(Intersectable i);
	public Rectangle getRectangle();
}
