package com.frostedberry.onegameamonth.onegam.world;

import com.frostedberry.onegameamonth.core.tick.Tick;

public abstract class WorldObject {
	protected World world;
	
	public WorldObject(World world) {
		this.world = world;
	}
	
	abstract void render();
	abstract void tick(Tick tick);
}
