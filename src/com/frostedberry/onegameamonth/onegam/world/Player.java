package com.frostedberry.onegameamonth.onegam.world;

import java.awt.Rectangle;

import org.lwjgl.input.Keyboard;

import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.tick.KeyEvent;
import com.frostedberry.onegameamonth.core.tick.Tick;

public class Player extends WorldObject implements Intersectable {
	protected float height;
	protected float heightOffset;
	protected int state;
	protected int animationCounter;
	protected int animationFrame;
	protected float heightSpeed = 2;
	protected int keyUp;
	protected int keyDown;
	protected boolean dead;
	protected boolean renderUp;
	protected boolean renderDown;
	protected int renderKeysTick;
	protected int renderKeyState;
	protected String keyState;
	protected boolean up;
	
	public Player(World world) {
		super(world);
		height = 125;
		keyUp = Keyboard.KEY_UP;
		keyDown = Keyboard.KEY_DOWN;
		renderUp = true;
		renderDown = true;
		keyState = "up";
	}
	
	public Player(World world, int keyUp, int keyDown) {
		super(world);
		this.world = world;
		this.keyUp = keyUp;
		this.keyDown = keyDown;
		renderUp = true;
		renderDown = true;
		keyState = "up";
	}
	
	@Override
	public void render() {
		Render.renderTexturePixel("skateboard_0", 64, height - heightOffset, 8, 16);
		Render.renderTexturePixel("player_dark_" + state + "_" + animationFrame, 64, height - heightOffset, 8, 16);
		
		if(renderUp) {
			Render.renderTexturePixel("key_" + keyState, 82, height + 5 - heightOffset - 9, 8);
			Render.renderTexturePixel("font_" + Keyboard.getKeyName(keyUp).toLowerCase(), 82, height + 5 - heightOffset - 9 + renderKeyState, 8);
		}
		
		if(renderDown) {
			Render.renderTexturePixel("key_" + keyState, 82, height + 5 - heightOffset + 9, 8);
			Render.renderTexturePixel("font_" + Keyboard.getKeyName(keyDown).toLowerCase(), 82, height + 5 - heightOffset + 9 + renderKeyState, 8);
		}
	}
	
	@Override
	public void tick(Tick tick) {
		if(animationCounter >= 8 / (world.getMoveSpeed() / 2)) {
			animationCounter = 0;
			if(animationFrame < 1) animationFrame++;
			else animationFrame = 0;
		}
		else animationCounter++;
		
		state = 0;
		
		for(KeyEvent ke : tick.getKeyEvents()) {
			if(ke.key == keyDown && ke.keyDown) up = false;
			else if(ke.key == keyUp && ke.keyDown) up = true;
		}
		
		if(up) {
			state = 2;
			int height = 30;
			if(heightOffset < height) heightOffset += heightSpeed;
			if(heightOffset > height) heightOffset = height;
		}
		else {
			state = 0;
			if(heightOffset > 0) heightOffset -= heightSpeed;
			if(heightOffset < 0) heightOffset = 0;
		}
		
		/*
		if(Keyboard.isKeyDown(keyDown)) {
			state = 1;
			if(heightOffset > 0) heightOffset -= heightSpeed;
			if(heightOffset < 0) heightOffset = 0;
		}
		else if(Keyboard.isKeyDown(keyUp)) {
			state = 2;
			int height = 30;
			if(heightOffset < height) heightOffset += heightSpeed;
			if(heightOffset > height) heightOffset = height;
		}
		else {
			if(heightOffset > 0) heightOffset -= heightSpeed;
			if(heightOffset < 0) heightOffset = 0;
		}
		*/
		
		for(Wall wall : world.getWalls()) {
			if(wall.intersects(this)) {
				dead = true;
				world.getGame().onPlayerHitIntersectable(this, wall);
				wall.r = 1;
				wall.g = 0;
				wall.b = 0;
			}
		}
		
		if(renderUp || renderDown) {
			if(renderKeysTick > 30) {
				renderKeysTick = 0;
				if(renderKeyState == 0) renderKeyState = 1;
				else renderKeyState = 0;
			}
			else renderKeysTick++;
			
			keyState = "up";
			if(renderKeyState == 1) keyState = "down";
			
			if(renderDown) {
				renderDown = !Keyboard.isKeyDown(keyDown);
			}
			
			if(renderUp) {
				renderUp = !Keyboard.isKeyDown(keyUp);
			}
		}
	}

	@Override
	public boolean intersects(Intersectable i) {
		return i.getRectangle().intersects(getRectangle());
	}

	@Override
	public Rectangle getRectangle() {
		return new Rectangle(64 - 4, (int) (height - heightOffset - 6), 4 * 2, 21);
	}
	
	public boolean isDead() {
		return dead;
	}
}
