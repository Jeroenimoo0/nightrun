package com.frostedberry.onegameamonth.onegam.world;

import java.util.ArrayList;
import java.util.List;

import com.frostedberry.onegameamonth.core.common.Color;
import com.frostedberry.onegameamonth.core.common.Random;
import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.tick.Tick;
import com.frostedberry.onegameamonth.onegam.Main;
import com.frostedberry.onegameamonth.onegam.games.Game;
import com.frostedberry.onegameamonth.onegam.world.Coin;

public class World {
	private float moonX;
	private Game game;
	
	private List<Building> buildings;
	private List<Star> stars;
	private List<Cloud> clouds;
	private List<Pole> poles;
	private List<Wall> walls;
	private List<Player> players;
	private List<Coin> coins;
	
	private Color buildingColor;
	private float buildingWaitLength;
	private float buildingMoveSpeed = 0.5f;
	
	private float starWaitLength;
	private float starMoveSpeed = 0.25f;
	
	private float cloudWaitLength;
	private float cloudMoveSpeed = 0.35f;
	
	private float poleWaitLength;
	private float poleMoveSpeed = 0.8f;
	
	private float wallWaitLength;
	private float wallMoveSpeed = 0.8f;
	private int pastWalls;
	
	private int pastWallsLastCoin;
	
	private float moveSpeed = 1f;
	
	public World(Game game) {
		this.game = game;
		buildings = new ArrayList<>();
		stars = new ArrayList<>();
		clouds = new ArrayList<>();
		poles = new ArrayList<>();
		walls = new ArrayList<>();
		players = new ArrayList<>();
		coins = new ArrayList<>();
		
		removeBuildings = new ArrayList<>();
		removeStars = new ArrayList<>();
		removeClouds = new ArrayList<>();
		removePoles = new ArrayList<>();
		removeWalls = new ArrayList<>();
		removeCoins = new ArrayList<>();
		
		buildingColor = new Color(0.3f, 0.3f, 0.3f);
		
		moonX = 336;
		
		stars.add(new Star(this, 100));
		stars.add(new Star(this, 200));
		
		poles.add(new Pole(this, 340 - 64));
		poles.add(new Pole(this, 340 - 64 * 2));
		poles.add(new Pole(this, 340 - 64 * 3));
		poles.add(new Pole(this, 340 - 64 * 4));
		poles.add(new Pole(this, 340 - 64 * 5));
		
		players.add(new Player(this));
		
		pastWallsLastCoin = 0;
	}
	
	int c;
	int img;
	
	int ph;
	
	public void render() {
		Render.renderColor(160, 90, 0, 160, 90, 0.5f, 0.5f, 0.5f);
		Render.renderColor(160, 155, 0, 160, 25, buildingColor);
		//Render.renderColor(160, 130, 0, 160, 4, 0.15f, 0.15f, 0.15f);
		
		int quality = ((Main)(game.gui.core)).quality.toInt();
		
		if(quality > 0) Render.renderTexture("moon", moonX, 18, 16);
		
		if(quality > 0) {
			for(Star s : stars) {
				s.render();
			}
		}
		
		if(quality > 1) {
			for(Cloud c : clouds) {
				c.render();
			}
		}
		
		for(Building b : buildings) {
			b.renderBuildings();
		}
		
		for(Building b : buildings) {
			b.renderWindows();
		}
		
		if(quality > 0) {
			for(Pole pole : poles) {
				pole.render();
			}
		}
		
		//Render bridge
		Render.renderColor(160, 180 - 32 - 4, 0, 160, 4, 0.2f, 0.2f, 0.2f);
		
		for(Wall wall : walls) {
			wall.render();
		}
		
		for(Coin coin : coins) {
			coin.render();
		}
		
		for(Player player : players) {
			player.render();
		}
		
		if(quality > 0) Render.renderTexture("screen_overlay", 160, 90, 256, 128);
		
		//Render.renderString("Speed: " + moveSpeed, 2, 170, 8);
	}
	
	public void tick(Tick tick) {
		/*
		for(KeyEvent ke : tick.getKeyEvents()) {
			if(ke.key == Keyboard.KEY_K && ke.keyDown) {
				moveSpeed += 0.500f;
			}
			else if(ke.key == Keyboard.KEY_M && ke.keyDown) {
				moveSpeed -= 0.500f;
			}
		}
		*/
		
		int quality = ((Main)(game.gui.core)).quality.toInt();
		
		if(quality > 0) {
			tickStars(tick);
			tickPoles(tick);
			moonX -= 0.1 * moveSpeed;
			if(moonX + 16 <= 0) moonX = 336;
		}
		if(quality > 1) {
			tickClouds(tick);
		}
		
		tickBuildings(tick);
		tickWalls(tick);
		tickCoins(tick);
		tickPlayers(tick);
	}
	
	private List<Integer> removeCoins; 
	
	private void tickCoins(Tick tick) {
		if(moveSpeed > 2.5f && pastWallsLastCoin <= 0) {
			if((int)wallWaitLength == 40) {
				if(Random.nextInt((int) (moveSpeed * 6)) == 0) {
					Coin coin = new Coin(this);
					
					coin.x = 320 + 4;
					coin.y = 128;
					if(Random.nextInt(2) == 0) coin.y -= 25;
					
					coins.add(coin);
					pastWallsLastCoin = 50;
				}
			}
		}
		else pastWallsLastCoin--;
		
		for(int i = 0; i < coins.size(); i++) {
			Coin coin = coins.get(i);
			coin.x -= getWallMoveSpeed();
			if(coin.x + 4 < 0) removeCoins.add(i);
		}
		
		for(int i : removeCoins) {
			coins.remove(i);
		}
		
		removeCoins.clear();
		
		for(Coin coin : coins) {
			coin.tick(tick);
		}
	}
	
	private void tickPlayers(Tick tick) {
		for(Player player : players) {
			player.tick(tick);
		}
	}
	
	private List<Integer> removeWalls;
	
	private void tickWalls(Tick tick) {
		if(wallWaitLength <= 0) {
			Wall wall = new Wall(this);
			
			if(Random.nextInt(2) == 0) wall.height = 25;
			wall.x = 320 + 4;
			
			if(game.getMain().quality.toInt() > 0) {
				int min = pastWalls / 20;
				if(min > game.getMain().difficulty.toInt()) min = game.getMain().difficulty.toInt();
				if(Random.nextInt((int) (moveSpeed * 6) - min) <= 1) {
					wall.changeHeight = true;
				}
			}
			
			wallWaitLength = 80;
			
			walls.add(wall);
		}
		else wallWaitLength -= getWallMoveSpeed();
		
		for(int i = 0; i < walls.size(); i++) {
			Wall wall = walls.get(i);
			wall.x -= getWallMoveSpeed();
			if(wall.x + 4 < 0) removeWalls.add(i);
		}
		
		for(int i : removeWalls) {
			walls.remove(i);
		}
		
		removeWalls.clear();
		
		for(Wall wall : walls) {
			wall.tick(tick);
		}
	}
	
	private List<Integer> removePoles;
	
	private void tickPoles(Tick tick) {
		if(poleWaitLength <= 0) {
			poleWaitLength = 64;
			
			Pole pole = new Pole(this);
			pole.x = 340;
			
			poles.add(pole);
		}
		else {
			poleWaitLength -= getPoleMoveSpeed();
		}
		
		for(int i = 0; i < poles.size(); i++) {
			Pole pole = poles.get(i);
			pole.x -= getPoleMoveSpeed();
			if(pole.x + 20< 0) removePoles.add(i);
		}
		
		for(int i : removePoles) {
			poles.remove(i);
		}
		
		removePoles.clear();
		
		for(Pole p : poles) {
			p.tick(tick);
		}
	}
	
	private List<Integer> removeClouds;
	
	private void tickClouds(Tick tick) {
		if(cloudWaitLength <= 0) {
			Cloud cloud = new Cloud(this);
			cloudWaitLength = cloud.width + Random.nextInt(21) + 40;
			cloud.height = Random.nextInt(31) + 20;
			cloud.x = 320 + cloud.width * 2 + Random.nextInt(6);
			
			clouds.add(cloud);
		}
		else {
			cloudWaitLength -= getCloudMoveSpeed();
		}
		
		for(int i = 0; i < clouds.size(); i++) {
			Cloud cloud = clouds.get(i);
			cloud.x -= getCloudMoveSpeed();
			if(cloud.x + cloud.width <= 0) removeClouds.add(i);
		}
		
		for(int i : removeClouds) {
			clouds.remove(i);
		}
		removeClouds.clear();
		
		for(Cloud cloud : clouds) {
			cloud.tick(tick);
		}
	}
	
	private List<Integer> removeStars;
	
	private void tickStars(Tick tick) {
		if(starWaitLength <= 0) {
			if(Random.nextInt(30) == 0) {
				Star star = new Star(this);
				starWaitLength = Random.nextInt(21) + 40;
				star.setX(310 - Random.nextInt(11));
				
				stars.add(star);
			}
		}
		else {
			starWaitLength -= getStarMoveSpeed();
		}
		
		for(int i = 0; i < stars.size(); i++) {
			Star star = stars.get(i);
			star.x -= getStarMoveSpeed();
			if(star.x + 12 <= 0) removeStars.add(i);
		}
		
		for(int i : removeStars) {
			stars.remove(i);
		}
		removeStars.clear();
		
		for(Star s : stars) {
			s.tick(tick);
		}
	}
	
	private List<Integer> removeBuildings;
	
	private void tickBuildings(Tick tick) {
		if(buildingWaitLength <= 0) {
			Building b = new Building(this);
			buildingWaitLength = b.getWidth() * 2 + Random.nextInt(20);
			if(((Main)(game.gui.core)).quality.toInt() == 0) buildingWaitLength += 50;
			b.setX(320 + (b.getWidth() + getBuildingMoveSpeed()) * 2);
			
			buildings.add(b);
		}
		else {
			buildingWaitLength -= getBuildingMoveSpeed();
		}
		
		for(int i = 0; i < buildings.size(); i++) {
			Building b = buildings.get(i);
			b.x -= getBuildingMoveSpeed();
			if(b.x + b.getWidth() * 2 <= 0) removeBuildings.add(i);
		}
		
		for(int i : removeBuildings) {
			buildings.remove(i);
		}
		
		removeBuildings.clear();
		
		for(Building b : buildings) {
			b.tick(tick);
		}
	}
	
	public int getStandartBuildingHeight() {
		return 130;
	}
	
	public Color getBuildingColor() {
		return buildingColor;
	}

	public float getPoleMoveSpeed() {
		return poleMoveSpeed * moveSpeed;
	}

	public float getCloudMoveSpeed() {
		return cloudMoveSpeed * moveSpeed;
	}

	public float getStarMoveSpeed() {
		return starMoveSpeed * moveSpeed;
	}

	public float getBuildingMoveSpeed() {
		return buildingMoveSpeed * moveSpeed;
	}

	public float getWallMoveSpeed() {
		return wallMoveSpeed * moveSpeed;
	}
	
	public float getMoveSpeed() {
		return moveSpeed;
	}
	
	public List<Wall> getWalls() {
		return walls;
	}
	
	public Game getGame() {
		return game;
	}
	
	public List<Player> getPlayers() {
		return players;
	}
	
	public void setMoveSpeed(float moveSpeed) {
		this.moveSpeed = moveSpeed;
	}
	
	public boolean arePlayersDead() {
		for(Player player : players) {
			if(!player.isDead()) return false;
		}
		return true;
	}
}
