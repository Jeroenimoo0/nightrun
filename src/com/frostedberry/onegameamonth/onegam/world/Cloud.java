package com.frostedberry.onegameamonth.onegam.world;

import com.frostedberry.onegameamonth.core.common.Random;
import com.frostedberry.onegameamonth.core.common.SineWiggler;
import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.tick.Tick;

public class Cloud extends WorldObject {
	protected float x;
	protected float height;
	protected float width;
	protected CloudPart[] cloudParts;
	
	public Cloud(World world, float x, float height) {
		super(world);
		this.x = x;
		this.height = height;
		
		init();
	}
	
	public Cloud(World world) {
		super(world);
		init();
	}
	
	private void init() {
		cloudParts = new CloudPart[5];
		
		for(int c = 0; c < cloudParts.length; c++) {
			CloudPart cloudPart = new CloudPart();
			
			cloudPart.yo = Random.nextInt(9) - 4;
			
			float xo = Math.abs(Random.nextInt(cloudParts.length / 2 * 5) + (cloudParts.length / 2 - c) * 5);
			if(c > cloudParts.length / 2) cloudPart.xo += xo;
			else cloudPart.xo -= xo;
			
			float color = (Random.nextInt(21) + 80f) / 100;
			cloudPart.color = color;
			
			cloudPart.s = Random.nextInt(3) + 5;
			
			cloudParts[c] = cloudPart;
		}
		
		for(CloudPart cp : cloudParts) {
			if(cp.xo + cp.s > width) width = cp.xo + cp.s;
		}
	}
	
	@Override
	public void render() {
		for(CloudPart cp : cloudParts) {
			Render.renderColor(x + cp.xo, height + cp.yo + cp.yoyo, 0, cp.s, cp.s, cp.color, cp.color, cp.color, .5f);
		}
	}
	
	@Override
	public void tick(Tick tick) {
		for(CloudPart cp : cloudParts) {
			cp.tick();
		}
	}
	
	private class CloudPart {
		protected float xo;
		protected float yo;
		protected float yoyo;
		protected float color;
		protected float s;
		protected SineWiggler wiggler;
		
		public CloudPart() {
			wiggler = new SineWiggler(2, 0.05f, Random.nextInt(10));
		}
		
		public void tick() {
			wiggler.next();
			yoyo = (float) wiggler.getCurrent();
		}
	}
}
