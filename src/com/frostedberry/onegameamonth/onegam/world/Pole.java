package com.frostedberry.onegameamonth.onegam.world;

import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.tick.Tick;

public class Pole extends WorldObject {
	protected float x;
	
	public Pole(World world) {
		super(world);
	}
	
	public Pole(World world, float x) {
		super(world);
		this.x = x;
	}
	
	@Override
	public void render() {
		Render.renderTexture("pole", x + 20, 180 - 16, 16, 16, 1, 1, 1);
		Render.renderTexture("pole_blur", x, 180 - 16, 16, 16, 1, 1, 1);
	}
	
	@Override
	public void tick(Tick tick) {
		
	}
}
