package com.frostedberry.onegameamonth.onegam.games;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import com.frostedberry.onegameamonth.core.audio.AudioPlayer;
import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.tick.KeyEvent;
import com.frostedberry.onegameamonth.core.tick.Tick;
import com.frostedberry.onegameamonth.onegam.Main;
import com.frostedberry.onegameamonth.onegam.Quality;
import com.frostedberry.onegameamonth.onegam.gui.GuiInGame;
import com.frostedberry.onegameamonth.onegam.gui.mainMenu.GuiMainMenu;
import com.frostedberry.onegameamonth.onegam.world.Intersectable;
import com.frostedberry.onegameamonth.onegam.world.Player;
import com.frostedberry.onegameamonth.onegam.world.World;

public class GameEndlessRun extends Game {
	protected int score;
	protected int bestScore;
	protected boolean gameOver;
	protected boolean newRecord;
	protected float maxSpeed;
	protected float speedR;
	protected int pastWalls;
	protected float scoreXo;
	protected float scoreXoSub;
	protected float startSpeed;
	
	protected boolean countDown;
	protected int countDownCounter;
	protected int countDownTicks;
	
	protected List<WindowButton> pauseMenuButtons;
	
	public GameEndlessRun(GuiInGame gui, Main main) {
		super(gui, main);
		resetWorld();
		
		pauseMenuButtons = new ArrayList<>();
		windowsInit();
		
		maxSpeed = 3 + main.quality.toInt();
		startSpeed = 1;
		
		world.setMoveSpeed(startSpeed);
	}
	
	private void windowsInit() {
		pauseMenuButtons.add(new WindowButton(160, 90) {
			@Override
			public void render() {
				super.render();
				Render.renderStringCentered("Main", x, y - 8, 8, textBrightness, textBrightness, textBrightness);
				Render.renderStringCentered("Menu", x, y , 8, textBrightness, textBrightness, textBrightness);
			}
			
			@Override
			public void onClick() {
				super.onClick();
				gui.setGui(new GuiMainMenu(gui.core));
			}
		});
		
		pauseMenuButtons.add(new TwoWindowButton(220, 90) {
			protected boolean high;
			protected boolean medium;
			protected boolean low;
			
			@Override
			public void init() {
				high = ((Main)gui.core).quality.toInt() == 2;
				medium = ((Main)gui.core).quality.toInt() == 1;
				low = ((Main)gui.core).quality.toInt() == 0;
			}
			
			@Override
			public void render() {
				super.render();
				Render.renderStringCentered("Quality", x - 12, y - 18, 8, textBrightness, textBrightness, textBrightness);
				
				Render.renderTexturePixel("checkbox_" + high, x - 24, y - 5, 4, 4, textBrightness, textBrightness, textBrightness);
				Render.renderTexturePixel("checkbox_" + medium, x - 24, y + 4, 4, 4, textBrightness, textBrightness, textBrightness);
				Render.renderTexturePixel("checkbox_" + low, x - 24, y + 13, 4, 4, textBrightness, textBrightness, textBrightness);
				
				Render.renderString("high", x - 18, y - 8, 6, textBrightness, textBrightness, textBrightness);
				Render.renderString("medium", x - 18, y + 1, 6, textBrightness, textBrightness, textBrightness);
				Render.renderString("low", x - 18, y + 10, 6, textBrightness, textBrightness, textBrightness);
			}
			
			@Override
			public void onClick() {
				float over = y - getMouseOnScreenY();
				
				if(over <= 9 && over >= 0) {
					((Main)gui.core).setQuality(Quality.High);
					((Main)gui.core).saveData();
					init();
				}
				else if(over <= 0 && over >= -9) {
					((Main)gui.core).setQuality(Quality.Medium);
					((Main)gui.core).saveData();
					init();
				}
				else if(over <= -9 && over >= -18) {
					((Main)gui.core).setQuality(Quality.Low);
					((Main)gui.core).saveData();
					init();
				}
			}
		});
		
		pauseMenuButtons.add(new TwoWindowButton(100, 90) {
			protected float volume;
			protected boolean dragging;
			
			@Override
			public void init() {
				volume = AudioPlayer.getVolume();
			}
			
			@Override
			public void tick(Tick tick) {
				super.tick(tick);
				
				float yOver = y - getMouseOnScreenY();
				float xOver = x - getMouseOnScreenX();
				
				if(xOver <= 30 && xOver >= -30) {
					if(yOver >= -8 && yOver <= 1.5f) {
						if(Mouse.isButtonDown(0)) {
							dragging = true;
							changeVolume((30 - xOver) / 60);
						}
					}
				}
				
				if(Mouse.isButtonDown(0) && dragging) {
					if(xOver <= -30) changeVolume(1);
					else if(xOver >= 30) changeVolume(0);
					
					if(brightness > 0.5f) brightness -= 0.1f;
					if(textBrightness < 1f) textBrightness += 0.1f;
				}
				else {
					if(dragging) {
						((Main)gui.core).saveData();
						dragging = false;
					}
				}
			}
			
			@Override
			public void render() {
				super.render();
				Render.renderStringCentered("Volume", x + 0, y - 17, 8, textBrightness, textBrightness, textBrightness);
				
				Render.renderColor(x, y, 0, 30, 5, textBrightness, textBrightness, textBrightness, 0.2f);
				Render.renderColor(x, y, 30, 1, textBrightness, textBrightness, textBrightness);
				Render.renderColor(x - 30 + volume * 60, y, 1, 5, textBrightness, textBrightness, textBrightness);
			
				Render.renderStringCentered("" + (int)(volume * 100), x + 0, y + 8, 8, textBrightness, textBrightness, textBrightness);
			}
			
			private void changeVolume(float f) {
				volume = f;
				((Main)gui.core).setVolume(f);
			}
		});
	}
	
	@Override
	public void init() {
		bestScore = ((Main)gui.core).getHighScore("endless");
	}
	
	@Override
	public void tickGame(Tick tick) {
		if(gameOver) {
			if(!isPaused()) {
				for(KeyEvent ke : tick.getKeyEvents()) {
					if(ke.key == Keyboard.KEY_R && ke.keyDown) resetWorld();
				}
			}
		}
		
		
		if(score > bestScore) {
			newRecord = true;
			bestScore = score;
		}
		
		if(pastWalls >= 5) {
			if(scoreXo < 40) {
				scoreXo += scoreXoSub;
				scoreXoSub += 0.2f;
			}
		}
		
		if(countDown) {
			if(countDownTicks >= 60) {
				countDownTicks = 0;
				countDownCounter--;
				if(countDownCounter == 0) {
					countDown = false;
					shouldTick = true;
				}
			}
			else countDownTicks++;
		}
		
		if(isPaused()) {
			for(WindowButton wb : pauseMenuButtons) {
				wb.tick(tick);
			}
		}
	}


	@Override
	public void onPause() {
		shouldTick = false;
		countDown = false;
	}

	@Override
	public void onUnPause() {
		if(!gameOver) {
			countDown = true;
			countDownTicks = 0;
			countDownCounter = 3;
		}
	}
	
	@Override
	public void renderGame() {
		if(gameOver) {
			//Render.renderString("Press R to restart", 160 - 9 * 8 - 1, 82 + 1, 8, 0, 0, 0);
			Render.renderColor(160, 90, 0, 60, 8, 0.2f, 0.2f, 0.2f, 0.5f);
			Render.renderString("Press R to restart", 160 - 55, 85, 8);
		}
		
		Render.renderColor(20 - scoreXo, 155, 20, 7, 0.8f, 0.8f, 0.8f);
		Render.renderString("Score", 2  - scoreXo, 152, 8, 0, 0, 0);
		
		Render.renderColor((score + "").toString().length() * 8, 172, (score + "").toString().length() * 8, 8, 0.8f, 0.8f, 0.8f);
		Render.renderString(score + "", 2, 170, 8, 0, 0, 0);
		
		Render.renderColor(300 + scoreXo, 155, 20, 7, 0.8f, 0.8f, 0.8f);
		Render.renderString("Best", 290 + scoreXo, 152, 8, 0, 0, 0);
		
		Render.renderColor(320 - (bestScore + "").toString().length() * 8, 172, (bestScore + "").toString().length() * 8, 8, 0.8f, 0.8f, 0.8f);
		Render.renderString(bestScore + "", 320 - (bestScore + "").toString().length() * 8, 170, 8, 0, 0, 0);
		
		Render.renderColor(160, 172, 50, 8, 0.8f, 0.8f, 0.8f);
		Render.renderColor(160, 172, 45, 4, 0, 0, 0);
		Render.renderColor(160, 172, (world.getMoveSpeed() - startSpeed / 2) / (maxSpeed - startSpeed / 2) * 90 / 2, 4, 1, speedR, speedR);
		
		if(countDown) {
			//Render.renderColor(160, 90, 0, 160, 90, 0, 0, 0, 0.2f);
			Render.renderStringCentered("" + countDownCounter, 160, 90, 16, 1, 1, 1);
		}
		
		if(isPaused()) {
			Render.renderColor(160, 90, 0, 160, 90, 0, 0, 0, 0.2f);
			Render.renderStringCentered("Paused", 160, 2, 16, 1, 1, 1);
			
			Render.renderStringCentered("< Escape", 290, 2, 8, 1, 1, 1);
			
			for(WindowButton wb : pauseMenuButtons) {
				wb.render();
			}
		}
	}
	

	@Override
	public void onGameOver() {
		if(newRecord) {
			((Main)gui.core).updateHighScore("endless", bestScore);
			((Main)gui.core).saveData();
		}
	}

	@Override
	public void onPlayerHitIntersectable(Player player, Intersectable i) {
		gameOver = true;
		shouldTick = false;
		onGameOver();
	}
	
	private int passCounter;
	
	@Override
	public void onIntersectablePass(Player player, Intersectable i) {
		score++;
		passCounter++;
		pastWalls++;
		if(passCounter >= 10) {
			if(world.getMoveSpeed() < maxSpeed) {
				world.setMoveSpeed(world.getMoveSpeed() + 0.5f);
				passCounter = 0;
				
				if(world.getMoveSpeed() == maxSpeed) speedR = 0.5f;
			}
		}
	}
	
	public void resetWorld() {
		gameOver = false;
		shouldRender = true;
		shouldTick = true;
		world = new World(this);
		world.setMoveSpeed(startSpeed);
		score = 0;
		newRecord = false;
		speedR = 1;
		scoreXo = 0;
		scoreXoSub = 0;
		pastWalls = 0;
	}
}
