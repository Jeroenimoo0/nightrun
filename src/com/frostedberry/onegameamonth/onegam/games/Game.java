package com.frostedberry.onegameamonth.onegam.games;

import com.frostedberry.onegameamonth.core.audio.AudioPlayer;
import com.frostedberry.onegameamonth.core.tick.Tick;
import com.frostedberry.onegameamonth.onegam.Main;
import com.frostedberry.onegameamonth.onegam.gui.GuiInGame;
import com.frostedberry.onegameamonth.onegam.world.Intersectable;
import com.frostedberry.onegameamonth.onegam.world.Player;
import com.frostedberry.onegameamonth.onegam.world.World;

public abstract class Game {
	public GuiInGame gui;
	protected Main main;
	protected World world;
	protected boolean shouldRender;
	protected boolean shouldTick;
	
	private boolean paused;
	
	public Game(GuiInGame gui, Main main) {
		this.gui = gui;
		shouldRender = true;
		shouldTick = true;
		this.main = main;
		
		init();
		
		AudioPlayer.playAsMusic("soundtrack", 1, 1, true);
		AudioPlayer.changeVolume("soundtrack", AudioPlayer.getVolume());
	}
	
	abstract public void init();
	abstract public void tickGame(Tick tick);
	abstract public void renderGame();
	abstract public void onPlayerHitIntersectable(Player player, Intersectable i);
	abstract public void onIntersectablePass(Player player, Intersectable i);
	abstract public void onGameOver();
	abstract public void onPause();
	abstract public void onUnPause();
	
	public void render() {
		if(shouldRender) if(world != null) world.render();
	}
	
	public void tick(Tick tick) {
		if(shouldTick) world.tick(tick);
	}
	
	public Main getMain() {
		return main;
	}
	
	public void pause() {
		paused = true;
	}
	
	public void unPause() {
		paused = false;
	}
	
	public boolean isPaused() {
		return paused;
	}

	public void switchPause() {
		paused = !paused;
		if(paused) onPause();
		else onUnPause();
	}
}
