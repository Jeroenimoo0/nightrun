package com.frostedberry.onegameamonth.onegam.games;

import com.frostedberry.onegameamonth.core.render.Render;

public class TwoWindowButton extends WindowButton {

	public TwoWindowButton(int cx, int cy) {
		super(cx, cy);
	}
	
	@Override
	public void render() {
		Render.renderTexturePixel("window", x, y, 0, 40, 40, brightness, brightness, brightness);
		Render.renderColor(x, y, 34, 16, 1, 1, 1);
		Render.renderTexture("window", x, y, 0, 93, 40, brightness, brightness, brightness);
	}
	
	@Override
	public boolean isMouseOnWindow() {
		if(isInBounds(35, getMouseOnScreenX(), x)) {
			if(isInBounds(19, getMouseOnScreenY(), y)) {
				return true;
			}
		}
		return false;
	}
}
