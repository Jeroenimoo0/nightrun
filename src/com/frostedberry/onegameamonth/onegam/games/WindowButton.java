package com.frostedberry.onegameamonth.onegam.games;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import com.frostedberry.onegameamonth.core.audio.AudioPlayer;
import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.tick.MouseEvent;
import com.frostedberry.onegameamonth.core.tick.Tick;

public class WindowButton {
	protected float x;
	protected float y;
	protected float brightness;
	protected float textBrightness;
	
	public WindowButton(int x, int y) {
		init();
		this.x = x;
		this.y = y;
		brightness = 1;
	}
	
	public void init() {
		
	}
	
	public void onClick() {
		AudioPlayer.playAsSound("next", 1, 1, false);
	}
	
	public void tick(Tick tick) {
		if(isMouseOnWindow()) {
			if(brightness > 0.5f) brightness -= 0.1f;
			if(textBrightness < 1f) textBrightness += 0.1f;
		}
		else {
			if(brightness < 1f) brightness += 0.1f;
			if(textBrightness > 0f) textBrightness -= 0.1f;
		}
		
		if(isMouseOnWindow()) {
			for(MouseEvent me : tick.getMouseEvents()) {
				if(me.button == 0 && me.buttonDown) {
					onClick();
				}
			}
		}
	}
	
	public void render() {
		Render.renderTexturePixel("window", x, y, 40, 40, brightness, brightness, brightness);
	}
	
	public boolean isMouseOnWindow() {
		if(isInBounds(15, getMouseOnScreenX(), x)) {
			if(isInBounds(19, getMouseOnScreenY(), y)) {
				return true;
			}
		}
		return false;
	}
	
	public float getMouseOnScreenX() {
		return Mouse.getX() / (float)Display.getWidth() * (float)320;
	}
	
	public float getMouseOnScreenY() {
		return (1 - Mouse.getY() / (float)Display.getHeight()) * (float)180;
	}
	
	public boolean isInBounds(float length, float f1, float f2) {
		return Math.abs(Math.abs(f1) - Math.abs(f2)) <= length;
	}
}
