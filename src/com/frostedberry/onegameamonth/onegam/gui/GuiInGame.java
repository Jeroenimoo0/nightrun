package com.frostedberry.onegameamonth.onegam.gui;

import org.lwjgl.input.Keyboard;

import com.frostedberry.onegameamonth.core.Core;
import com.frostedberry.onegameamonth.core.gui.Gui;
import com.frostedberry.onegameamonth.core.tick.Tick;
import com.frostedberry.onegameamonth.core.tick.KeyEvent;
import com.frostedberry.onegameamonth.onegam.Main;
import com.frostedberry.onegameamonth.onegam.games.Game;
import com.frostedberry.onegameamonth.onegam.games.GameEndlessRun;

public class GuiInGame extends Gui {
	protected Game game;
	
	public GuiInGame(Core core) {
		super("GuiInGame", core);
		game = new GameEndlessRun(this, (Main)core);
	}

	@Override
	public Gui getNextGui() {
		return null;
	}

	@Override
	public void render() {
		
		game.render();
		game.renderGame();
	}

	@Override
	public void tick(Tick tick) {
		game.tick(tick);
		game.tickGame(tick);
		
		for(KeyEvent ke : tick.getKeyEvents()) {
			if((ke.key == Keyboard.KEY_ESCAPE || ke.key == Keyboard.KEY_BACK) && ke.keyDown) {
				game.switchPause();
			}
		}
	}
}
