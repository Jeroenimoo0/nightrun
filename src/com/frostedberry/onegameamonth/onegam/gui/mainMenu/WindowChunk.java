package com.frostedberry.onegameamonth.onegam.gui.mainMenu;

import java.util.ArrayList;
import java.util.List;

import com.frostedberry.onegameamonth.core.common.Random;

public class WindowChunk {
	protected Building b;
	protected int chunk;
	protected List<Window> windows;
	
	public WindowChunk(Building b, int chunk) {
		windows = new ArrayList<>();
		this.b = b;
		this.chunk = chunk;
		init();
	}
	
	public void onWindowLeave() {
		
	}
	
	public void init() {
		randomizeWindows();
	}
	
	public void add(Window w) {
		windows.add(w);
	}
	
	public void render() {
		for(Window w : windows) {
			w.render();
		}
	}
	
	public void tick() {
		for(Window w : windows) {
			w.tick();
		}
	}
	
	public void randomizeWindows() {
		for(int cx = 0; cx < 4; cx++) {
			for(int cy = 0; cy < 4; cy++) {
				if(Random.nextInt(4) == 0) {
					add(new Window(this, cx, cy));
				}
			}
		}
	}
}
