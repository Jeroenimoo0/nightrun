package com.frostedberry.onegameamonth.onegam.gui.mainMenu;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import com.frostedberry.onegameamonth.core.Core;
import com.frostedberry.onegameamonth.core.audio.AudioLoader;
import com.frostedberry.onegameamonth.core.audio.AudioPlayer;
import com.frostedberry.onegameamonth.core.common.SineMove;
import com.frostedberry.onegameamonth.core.gui.Gui;
import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.tick.KeyEvent;
import com.frostedberry.onegameamonth.core.tick.MouseEvent;
import com.frostedberry.onegameamonth.core.tick.Tick;

public class GuiMainMenu extends Gui {
	protected Building building;
	protected float yScroll;
	
	protected boolean scrolling;
	protected SineMove sineMove;
	protected float a;
	protected float b;
	
	protected boolean intro;
	protected boolean outro;
	protected Gui nextGui;
	
	protected List<Integer> prevGuis;
	
	protected float backVisability;
	protected float backColor;
	
	public GuiMainMenu(Core core) {
		super("GuiMainMenu", core);
		
		prevGuis = new ArrayList<>();
		
		building = new Building(this);
		
		yScroll = 550;
		scrollToWindow(-1);
		
		intro = true;
		
		if(!AudioLoader.getAudio("soundtrack").isPlaying()) AudioPlayer.playAsMusic("soundtrack", 1, 1, true);
	}

	@Override
	public Gui getNextGui() {
		return null;
	}

	@Override
	public void render() {
		Render.renderColor(160, 90, 160, 90, 0.5f, 0.5f, 0.5f);
		Render.renderColor(160, -300 + yScroll, 0, 160, 90, new float[]{0, 0, 0, 0}, new float[]{0, 0, 0, 0}, new float[]{0, 0, 0, 0}, new float[]{1, 1, 0, 0});
		Render.renderColor(160, -480 + yScroll, 0, 160, 90, 0, 0, 0);
		
		GL11.glPushMatrix();
		
		GL11.glTranslatef(0, yScroll, 0);
		
		building.render();
		
		GL11.glPopMatrix();
		
		Render.renderString("< backspace", 250, 2, 8, backColor, backColor, backColor, backVisability);
	}

	@Override
	public void tick(Tick tick) {
		for(KeyEvent ke : tick.getKeyEvents()) {
			if(isScrollAtWindow(-1)) {
				if(ke.keyDown && ke.key != Keyboard.KEY_F11 && ke.key != Keyboard.KEY_P) {
					scrollToWindow(1);
					AudioPlayer.playAsSound("next", 1, 1, false);
				}
			}
			else {
				if(ke.key == Keyboard.KEY_BACK && ke.keyDown) {
					scrollToPrevWindow();
					if(b < 180) backColor = 1;
					AudioPlayer.playAsSound("back", 1, 1, false);
				}
				else if(ke.key == Keyboard.KEY_R && ke.keyDown) {
					building.initWindows();
				}
			}
		}
		
		for(MouseEvent me : tick.getMouseEvents()) {
			if(!scrolling) {
				//if(me.scroll != 0) {
				//	yScroll += me.scroll / 10;
				//}
				
				if(me.button == 0 && me.buttonDown) {
					for(WindowChunk wc : building.windowChunks) {
						for(Window w : wc.windows) {
							if(w instanceof WindowButton) {
								if(w.isMouseOnWindow()) {
									((WindowButton) w).onClick();
								}
							}
						}
					}
				}
			}
		}
		
		building.tick();
		
		if(scrolling) {
			if(sineMove.isCompleted()) {
				yScroll = b;
				scrolling = false;
				
				for(Star s : building.stars) {
					s.moveTo(-yScroll + 20);
				}
			}
			
			yScroll = (float) ((b - a) * sineMove.getCurrentOne() + a);
			sineMove.next();
		}
		
		if(outro) {
			if(!scrolling) {
				setGui(nextGui);
			}
		}
		
		if(yScroll < 180 && b < 180) {
			if(backVisability < 1) backVisability += 0.1f;
		}
		else if(backVisability > 0) backVisability -= 0.1f; 
		
		if(backColor > 0) backColor -= 0.1f;
	}
	
	
	public void fadeToGui(Gui gui) {
		outro = true;
		scrollToWindow(-2);
		nextGui = gui;
	}
	
	public void scrollToWindow(int w) {
		if(!scrolling) {
			b = -w * building.CHUNKHEIGHT;
			if(w == -1) b = 180;
			else if(w == -2) b = 550;
			
			if(b != yScroll) {
				scrolling = true;
				a = yScroll;
				sineMove = new SineMove(20);
				
				if(b < 180) prevGuis.add(w);
			}
		}
	}
	
	public void scrollToPrevWindow() {
		if(!scrolling) {
			int window = -1;
			
			if(prevGuis.size() > 1) {
				window = prevGuis.get(prevGuis.size() - 2);
				prevGuis.remove(prevGuis.size() - 2);
				prevGuis.remove(prevGuis.size() - 1);
			}
			
			b = -window * building.CHUNKHEIGHT;
			if(window == -1) b = 180;
			else if(window == -2) b = 550;
			
			if(b != yScroll) {
				scrolling = true;
				a = yScroll;
				sineMove = new SineMove(20);
			}
		}
	}
	
	public boolean isScrollAtWindow(int w) {
		if(w > 0) return yScroll == -w * building.CHUNKHEIGHT;
		else if(w == -1) return yScroll == 180;
		else if(w == -2) return yScroll == 550;
		else return false;
	}
	
	public float getMouseOnScreenX() {
		return Mouse.getX() / (float)Display.getWidth() * (float)core.getGameWidth();
	}
	
	public float getMouseOnScreenY() {
		return (1 - Mouse.getY() / (float)Display.getHeight()) * (float)core.getGameHeight();
	}
	
	public boolean isInBounds(float length, float f1, float f2) {
		return Math.abs(Math.abs(f1) - Math.abs(f2)) <= length;
	}
}
