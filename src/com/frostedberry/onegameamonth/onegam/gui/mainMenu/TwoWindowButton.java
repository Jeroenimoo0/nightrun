package com.frostedberry.onegameamonth.onegam.gui.mainMenu;

import com.frostedberry.onegameamonth.core.render.Render;

public class TwoWindowButton extends WindowButton {

	public TwoWindowButton(WindowChunk wc, int cx, int cy) {
		super(wc, cx, cy);
	}
	
	@Override
	public void render() {
		Render.renderTexturePixel("window", (x + 1) * 40 + 20, y * 40 + 25, 0, 40, 40, brightness, brightness, brightness);
		Render.renderColor((x + 0.5f) * 40 + 20, y * 40 + 24.2f, 34, 18, 1, 1, 1);
		Render.renderTexture("window", (x + 0.5f) * 40 + 20, y * 40 + 25, 0, 93, 40, brightness, brightness, brightness);
	}
	
	@Override
	public boolean isMouseOnWindow() {
		if(isInBounds(35, wc.b.gui.getMouseOnScreenX(), getOnScreenX())) {
			if(isInBounds(19, wc.b.gui.getMouseOnScreenY(), getOnScreenY())) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public float getOnScreenX() {
		return wc.b.buildingX - wc.b.BUILDINGWIDTH + (x + 0.5f) * 40f + 20f;
	}

}
