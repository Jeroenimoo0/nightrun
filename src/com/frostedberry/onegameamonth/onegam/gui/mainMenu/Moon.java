package com.frostedberry.onegameamonth.onegam.gui.mainMenu;

import com.frostedberry.onegameamonth.core.render.Render;

public class Moon {
	protected Building b;
	protected float x;
	protected final float MOONWIDTH = 80;
	
	public Moon(Building b) {
		this.b = b;
	}
	
	public Moon(Building b, float x) {
		this.b = b;
		this.x = x;
	}
	
	public void render() {
		Render.renderTexturePixel("moon", x, -60, MOONWIDTH);
	}
	
	public void tick() {
		x += 0.5f;
		if(x > 400) x = -80;
	}
}
