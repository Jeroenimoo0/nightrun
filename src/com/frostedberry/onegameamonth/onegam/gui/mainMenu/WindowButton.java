package com.frostedberry.onegameamonth.onegam.gui.mainMenu;

import com.frostedberry.onegameamonth.core.audio.AudioPlayer;
import com.frostedberry.onegameamonth.core.render.Render;

public class WindowButton extends Window {
	protected float brightness;
	protected float textBrightness;
	
	public WindowButton(WindowChunk wc, int cx, int cy) {
		super(wc, cx, cy);
		init();
	}
	
	public void init() {
		
	}
	
	public void onClick() {
		AudioPlayer.playAsSound("next", 1, 1, false);
	}
	
	public void tick() {
		if(isMouseOnWindow()) {
			if(brightness > 0.5f) brightness -= 0.1f;
			if(textBrightness < 1f) textBrightness += 0.1f;
		}
		else {
			if(brightness < 1f) brightness += 0.1f;
			if(textBrightness > 0f) textBrightness -= 0.1f;
		}
	}
	
	public void render() {
		Render.renderTexturePixel("window", x * 40 + 20, y * 40 + 25, 40, 40, brightness, brightness, brightness);
	}
}
