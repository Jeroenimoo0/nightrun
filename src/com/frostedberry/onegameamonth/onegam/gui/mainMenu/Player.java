package com.frostedberry.onegameamonth.onegam.gui.mainMenu;

import com.frostedberry.onegameamonth.core.render.Render;

public class Player {
	protected Building b;
	protected float x;
	protected float moonShineDarken;
	
	protected int walkTick;
	protected int textureState;
	protected int stillTick;
	
	public Player(Building b) {
		this.b = b;
		x = 80;
	}
	
	public void render() {
		Render.renderTexturePixel("player_skateboard_" + textureState, 172 - x, -66, 16, 32);
		Render.renderTexturePixel("player_skateboard_white_" + textureState, 172 - x, -66, 0, 16, 32, moonShineDarken, moonShineDarken, moonShineDarken);
	}
	
	public void tick() {
		if(x > 0) {
			x -= 0.5f;
			
			if(walkTick >= 10) {
				if(textureState == 0) textureState = 1;
				else textureState = 0;
				walkTick = 0;
			}
			else walkTick++;
			
			if(x == 0) {
				textureState = 2;
			}
		}
		else if(textureState != 3){
			if(stillTick >= 10) {
				textureState = 3;
				b.gui.intro = false;
			}
			stillTick++;
		}
		
		if(isInBounds(b.moon.MOONWIDTH, 172 - x, b.moon.x)) {
			moonShineDarken = (Math.abs(Math.abs(172 - x) - Math.abs(b.moon.x))) / b.moon.MOONWIDTH;
		}
	}
	
	public boolean isInBounds(float length, float f1, float f2) {
		return Math.abs(Math.abs(f1) - Math.abs(f2)) <= length;
	}
}
