package com.frostedberry.onegameamonth.onegam.gui.mainMenu;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import com.frostedberry.onegameamonth.core.audio.AudioPlayer;
import com.frostedberry.onegameamonth.core.common.SineWiggler;
import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.onegam.Difficulty;
import com.frostedberry.onegameamonth.onegam.Main;
import com.frostedberry.onegameamonth.onegam.Quality;
import com.frostedberry.onegameamonth.onegam.gui.intro.GuiGameIntro;

public class Building {
	protected GuiMainMenu gui;
	protected float length;
	protected float buildingX;
	
	protected final float CHUNKHEIGHT = 170;
	protected final float BUILDINGWIDTH = 80;
	
	protected Player player;
	protected Moon moon;
	
	protected SineWiggler logoWiggler;
	protected SineWiggler continueStringWiggler;
	
	protected float continueStringVisability;
	
	protected List<WindowChunk> windowChunks;
	protected List<Star> stars;
	
	public Building(GuiMainMenu gui) {
		this.gui = gui;
		
		windowChunks = new ArrayList<>();
		stars = new ArrayList<>();
	
		initWindows();
		
		logoWiggler = new SineWiggler(2, 0.05f);
		continueStringWiggler = new SineWiggler(1, 0.05f);
		
		player = new Player(this);
		moon = new Moon(this, 92);
		
		buildingX = 100;
		
		stars.add(new Star(this, 40));
		stars.add(new Star(this, 200));
		stars.add(new Star(this, 280));
	}
	
	protected void initWindows() {
		windowChunks.clear();
		
		windowChunks.add(new WindowChunk(this, 0));
		
		windowChunks.add(new WindowChunk(this, 1) {
			@Override
			public void init() {
				add(new WindowButton(this, 0, 0) {
					@Override
					public void render() {
						super.render();
						Render.renderStringCentered("Play", x * 40 + 20, y * 40 + 20, 8, textBrightness, textBrightness, textBrightness);
					}
					
					@Override
					public void onClick() {
						gui.scrollToWindow(7);
						super.onClick();
					}
				});
				
				add(new TwoWindowButton(this, 1, 1) {
					@Override
					public void render() {
						super.render();
						Render.renderStringCentered("Options", (x + 0.5f) * 40 + 20, y * 40 + 20, 8, textBrightness, textBrightness, textBrightness);
					}
					
					@Override
					public void onClick() {
						wc.b.gui.scrollToWindow(3);
						super.onClick();
					}
				});
				
				add(new TwoWindowButton(this, 2, 3) {
					@Override
					public void render() {
						super.render();
						Render.renderStringCentered("Credits", (x + 0.5f) * 40 + 20, y * 40 + 20, 8, textBrightness, textBrightness, textBrightness);
					}
					
					@Override
					public void onClick() {
						wc.b.gui.scrollToWindow(5);
						super.onClick();
					}
				});
				
				if(gui.core.isInBrowser()) add(new Window(this, 0, 3));
				else {
					add(new WindowButton(this, 0, 3) {
						@Override
						public void render() {
							super.render();
							Render.renderStringCentered("Quit", x * 40 + 20, y * 40 + 20, 8, textBrightness, textBrightness, textBrightness);
						}
						
						@Override
						public void onClick() {
							gui.setGui(null);
							if(Display.isFullscreen()) ((Main)gui.core).disableFullScreen();
							AudioPlayer.stopAllAudio();
						}
					});
				}
			}
		});
		
		windowChunks.add(new WindowChunk(this, 2));
		
		windowChunks.add(new WindowChunk(this, 3) {
			@Override
			public void init() {
				add(new TwoWindowButton(this, 0, 0) {
					protected boolean high;
					protected boolean medium;
					protected boolean low;
					
					@Override
					public void init() {
						high = ((Main)b.gui.core).quality.toInt() == 2;
						medium = ((Main)b.gui.core).quality.toInt() == 1;
						low = ((Main)b.gui.core).quality.toInt() == 0;
					}
					
					@Override
					public void render() {
						super.render();
						Render.renderStringCentered("Quality", x * 40 + 26, y * 40 + 7, 8, textBrightness, textBrightness, textBrightness);
						
						Render.renderTexturePixel("checkbox_" + high, x * 40 + 10, y * 40 + 20, 4, 4, textBrightness, textBrightness, textBrightness);
						Render.renderTexturePixel("checkbox_" + medium, x * 40 + 10, y * 40 + 29, 4, 4, textBrightness, textBrightness, textBrightness);
						Render.renderTexturePixel("checkbox_" + low, x * 40 + 10, y * 40 + 38, 4, 4, textBrightness, textBrightness, textBrightness);
						
						Render.renderString("high", x * 40 + 20, y * 40 + 17, 6, textBrightness, textBrightness, textBrightness);
						Render.renderString("medium", x * 40 + 20, y * 40 + 26, 6, textBrightness, textBrightness, textBrightness);
						Render.renderString("low", x * 40 + 20, y * 40 + 35, 6, textBrightness, textBrightness, textBrightness);
					}
					
					@Override
					public void onClick() {
						float over = getOnScreenY() - b.gui.getMouseOnScreenY();
						
						if(over <= 9 && over >= 0) {
							((Main)b.gui.core).setQuality(Quality.High);
							((Main)b.gui.core).saveData();
							init();
						}
						else if(over <= 0 && over >= -9) {
							((Main)b.gui.core).setQuality(Quality.Medium);
							((Main)b.gui.core).saveData();
							init();
						}
						else if(over <= -9 && over >= -18) {
							((Main)b.gui.core).setQuality(Quality.Low);
							((Main)b.gui.core).saveData();
							init();
						}
					}
				});
				
				add(new TwoWindowButton(this, 2, 1) {
					protected boolean hard;
					protected boolean normal;
					protected boolean easy;
					
					@Override
					public void init() {
						hard = ((Main)b.gui.core).difficulty.toInt() == 2;
						normal = ((Main)b.gui.core).difficulty.toInt() == 1;
						easy = ((Main)b.gui.core).difficulty.toInt() == 0;
					}
					
					@Override
					public void render() {
						super.render();
						Render.renderStringCentered("Difficulty", x * 40 + 33, y * 40 + 7, 8, textBrightness, textBrightness, textBrightness);
						
						Render.renderTexturePixel("checkbox_" + hard, x * 40 + 10, y * 40 + 20, 4, 4, textBrightness, textBrightness, textBrightness);
						Render.renderTexturePixel("checkbox_" + normal, x * 40 + 10, y * 40 + 29, 4, 4, textBrightness, textBrightness, textBrightness);
						Render.renderTexturePixel("checkbox_" + easy, x * 40 + 10, y * 40 + 38, 4, 4, textBrightness, textBrightness, textBrightness);
						
						Render.renderString("hard", x * 40 + 20, y * 40 + 17, 6, textBrightness, textBrightness, textBrightness);
						Render.renderString("normal", x * 40 + 20, y * 40 + 26, 6, textBrightness, textBrightness, textBrightness);
						Render.renderString("easy", x * 40 + 20, y * 40 + 35, 6, textBrightness, textBrightness, textBrightness);
					}
					
					@Override
					public void onClick() {
						float over = getOnScreenY() - b.gui.getMouseOnScreenY();
						
						if(over <= 9 && over >= 0) {
							((Main)b.gui.core).setDifficulty(Difficulty.Hard);
							((Main)b.gui.core).saveData();
							init();
						}
						else if(over <= 0 && over >= -9) {
							((Main)b.gui.core).setDifficulty(Difficulty.Normal);
							((Main)b.gui.core).saveData();
							init();
						}
						else if(over <= -9 && over >= -18) {
							((Main)b.gui.core).setDifficulty(Difficulty.Easy);
							((Main)b.gui.core).saveData();
							init();
						}
					}
				});
				
				add(new TwoWindowButton(this, 0, 2) {
					protected float volume;
					protected boolean dragging;
					
					@Override
					public void init() {
						volume = AudioPlayer.getVolume();
					}
					
					@Override
					public void tick() {
						super.tick();
						
						float yOver = getOnScreenY() - b.gui.getMouseOnScreenY();
						float xOver = getOnScreenX() - b.gui.getMouseOnScreenX();
						
						if(xOver <= 30 && xOver >= -30) {
							if(yOver >= -8 && yOver <= 1.5f) {
								if(Mouse.isButtonDown(0)) {
									dragging = true;
									changeVolume((30 - xOver) / 60);
								}
							}
						}
						
						if(Mouse.isButtonDown(0) && dragging) {
							if(xOver <= -30) changeVolume(1);
							else if(xOver >= 30) changeVolume(0);
							
							if(brightness > 0.5f) brightness -= 0.1f;
							if(textBrightness < 1f) textBrightness += 0.1f;
						}
						else {
							if(dragging) {
								((Main)b.gui.core).saveData();
								dragging = false;
							}
						}
					}
					
					@Override
					public void render() {
						super.render();
						Render.renderStringCentered("Volume", x * 40 + 40, y * 40 + 7, 8, textBrightness, textBrightness, textBrightness);
						
						Render.renderColor(x * 40 + 40, y * 40 + 24, 0, 30, 5, textBrightness, textBrightness, textBrightness, 0.2f);
						Render.renderColor(x * 40 + 40, y * 40 + 24, 30, 1, textBrightness, textBrightness, textBrightness);
						Render.renderColor(x * 40 + 10 + volume * 60, y * 40 + 24, 1, 5, textBrightness, textBrightness, textBrightness);
					
						Render.renderStringCentered("" + (int)(volume * 100), x * 40 + 40, y * 40 + 32, 8, textBrightness, textBrightness, textBrightness);
					}
					
					private void changeVolume(float f) {
						volume = f;
						((Main)b.gui.core).setVolume(f);
					}
				});
				
				add(new WindowButton(this, 3, 3) {
					protected boolean sure;
					protected int counter;
					protected int counterTicks;
					
					@Override
					public void render() {
						super.render();
						if(!sure)Render.renderStringCentered("Reset", x * 40 + 20.5f, y * 40 + 20, 8, textBrightness, textBrightness, textBrightness);
						else {
							Render.renderStringCentered("click", x * 40 + 20.5f, y * 40 + 10, 8, textBrightness, textBrightness, textBrightness);
							Render.renderStringCentered("again", x * 40 + 20.5f, y * 40 + 20, 8, textBrightness, textBrightness, textBrightness);
							Render.renderStringCentered("" + counter, x * 40 + 20.5f, y * 40 + 30, 8, textBrightness, textBrightness, textBrightness);
						}
					}
					
					@Override
					public void onClick() {
						if(!sure) {
							sure = true;
							counter = 10;
						}
						else {
							((Main)gui.core).resetHighScores();
							((Main)gui.core).saveData();
							
							sure = false;
						}
						
						super.onClick();
					}
					
					@Override
					public void tick() {
						super.tick();
						
						if(counterTicks > 60) {
							counter--;
							counterTicks = 0;
						}
						else counterTicks++;
						
						if(counter <= 0) sure = false;
					}
				});
				
				add(new WindowButton(this, 2, 3) {
					protected boolean sure;
					protected int counter;
					protected int counterTicks;
					
					@Override
					public void render() {
						super.render();
						if(!sure){
							Render.renderStringCentered("Reset", x * 40 + 20.5f, y * 40 + 15, 8, textBrightness, textBrightness, textBrightness);
							Render.renderStringCentered("Scores", x * 40 + 20.5f, y * 40 + 25, 6, textBrightness, textBrightness, textBrightness);
						}
						else {
							Render.renderStringCentered("click", x * 40 + 20.5f, y * 40 + 10, 8, textBrightness, textBrightness, textBrightness);
							Render.renderStringCentered("again", x * 40 + 20.5f, y * 40 + 20, 8, textBrightness, textBrightness, textBrightness);
							Render.renderStringCentered("" + counter, x * 40 + 20.5f, y * 40 + 30, 8, textBrightness, textBrightness, textBrightness);
						}
					}
					
					@Override
					public void onClick() {
						if(!sure) {
							sure = true;
							counter = 10;
						}
						else {
							((Main)gui.core).resetHighScores();
							((Main)gui.core).saveData();
							
							sure = false;
							for(Window w : wc.windows) {
								if(w instanceof WindowButton) {
									((WindowButton) w).init();
								}
							}
						}
						
						super.onClick();
					}
					
					@Override
					public void tick() {
						super.tick();
						
						if(counterTicks > 60) {
							counter--;
							counterTicks = 0;
						}
						else counterTicks++;
						
						if(counter <= 0) sure = false;
					}
				});
			}
		});
		
		windowChunks.add(new WindowChunk(this, 4));
		
		windowChunks.add(new WindowChunk(this, 5) {
			@Override
			public void init() {
				for(int cx = 0; cx < 4; cx++) {
					for(int cy = 0; cy < 4; cy++) {
						add(new Window(this, cx, cy));
					}
				}
			}
			
			@Override
			public void render() {
				super.render();
				
				Render.renderColor(1.5f * 40 + 20, 1.48f * 40 + 25, 0, 75, 78, 1, 1, 1);
				Render.renderStringCentered("biggest window/ Credits", 1.5f * 40 + 20, 8, 8, 0, 0, 0, 1);
			
				Render.renderString("Art and Code by @ Jeroenimoo0", 10, 30, 6, 0, 0, 0);
				Render.renderString("aka Jeroen van de Haterd.", 10, 40, 6, 0, 0, 0);
				
				Render.renderString("Soundtrack by @ filipinomike", 10, 50, 6, 0, 0, 0);
				
				Render.renderString("This game was created for ", 10, 65, 6, 0, 0, 0);
				Render.renderString("One Game a Month, the name says", 10, 75, 6, 0, 0, 0);
				Render.renderString("it all. A great way for game", 10, 85, 6, 0, 0, 0);
				Render.renderString("developers to get experience.", 10, 95, 6, 0, 0, 0);
				
				Render.renderString("Check out frostedberry.com for", 10, 110, 6, 0, 0, 0);
				Render.renderString("our latest games and more.", 10, 120, 6, 0, 0, 0);
				
				Render.renderString("Thanks to my friends, family and", 10, 135, 6, 0, 0, 0);
				Render.renderString("noodles, damn noodles are good.", 10, 145, 6, 0, 0, 0);
			}
		});
		
		windowChunks.add(new WindowChunk(this, 6));
		
		windowChunks.add(new WindowChunk(this, 7) {
			@Override
			public void init() {
				add(new TwoWindowButton(this, 0, 0) {
					@Override
					public void render() {
						super.render();
						Render.renderStringCentered("ENDLESS", x * 40 + 40, y * 40 + 10, 8, textBrightness, textBrightness, textBrightness);
						Render.renderStringCentered("*", x * 40 + 63, y * 40 + 9, 8, textBrightness, textBrightness, textBrightness);
						Render.renderStringCentered("Till the END!", x * 40 + 40, y * 40 + 23.5f, 6, textBrightness, textBrightness, textBrightness);
						Render.renderStringCentered("*until you die.", x * 40 + 40, y * 40 + 35, 6, textBrightness, textBrightness, textBrightness);
					}
					
					@Override
					public void onClick() {
						wc.b.gui.fadeToGui(new GuiGameIntro(wc.b.gui.core));
						super.onClick();
					}
				});
				
				add(new TwoWindowButton(this, 2, 1) {
					@Override
					public void render() {
						super.render();
						Render.renderStringCentered("Coming soon", x * 40 + 40, y * 40 + 10, 8, textBrightness, textBrightness, textBrightness);
					}
					
					@Override
					public void onClick() {
						wc.b.gui.fadeToGui(new GuiGameIntro(wc.b.gui.core));
						super.onClick();
					}
				});
				
				add(new TwoWindowButton(this, 0, 2) {
					@Override
					public void render() {
						super.render();
						Render.renderStringCentered("Also", x * 40 + 40, y * 40 + 10, 8, textBrightness, textBrightness, textBrightness);
						Render.renderStringCentered("Coming soon", x * 40 + 40, y * 40 + 20, 8, textBrightness, textBrightness, textBrightness);
					}
					
					@Override
					public void onClick() {
						wc.b.gui.fadeToGui(new GuiGameIntro(wc.b.gui.core));
						super.onClick();
					}
				});
				
				add(new TwoWindowButton(this, 2, 3) {
					@Override
					public void render() {
						super.render();
						Render.renderStringCentered("Also", x * 40 + 40, y * 40 + 10, 8, textBrightness, textBrightness, textBrightness);
						Render.renderStringCentered("Also", x * 40 + 40, y * 40 + 20, 8, textBrightness, textBrightness, textBrightness);
						Render.renderStringCentered("Coming soon", x * 40 + 40, y * 40 + 30, 8, textBrightness, textBrightness, textBrightness);
					}
					
					@Override
					public void onClick() {
						wc.b.gui.fadeToGui(new GuiGameIntro(wc.b.gui.core));
						super.onClick();
					}
				});
			}
		});
		
		windowChunks.add(new WindowChunk(this, 8));
		
		length = windowChunks.size() * CHUNKHEIGHT;
	}
	
	public void render() {
		//Render.renderColor(160, 0, 160, 1);
		
		renderWorld();
		
		player.render();
		
		renderBuilding();
		
		if(!gui.intro) Render.renderStringCentered("Press any key to continue", 160, -14 + (float)continueStringWiggler.getCurrent(), 8, 0, 0, 0, continueStringVisability);
		if(!gui.intro) Render.renderStringCentered("Really, even sound keys work...", 160, -5, 4, 0, 0, 0, continueStringVisability);
	}
	
	private void renderWorld() {
		moon.render();
		
		for(Star s : stars) {
			s.render();
		}
		
		Render.renderTexturePixel("game_logo", 160, (float) (-130 + logoWiggler.getCurrent()), 128, 32);
	}
	
	private void renderBuilding() {
		GL11.glPushMatrix();
		GL11.glTranslatef(buildingX, 0, 0);
		
		Render.renderColor(0, -20, BUILDINGWIDTH, 20, 0.3f, 0.3f, 0.3f);
		Render.renderColor(-BUILDINGWIDTH + 30, -50, 30, 10, 0.3f, 0.3f, 0.3f);
		Render.renderColor(-BUILDINGWIDTH + 30, -65, 20, 5, 0.3f, 0.3f, 0.3f);
		Render.renderColor(-BUILDINGWIDTH + 30, -90, 5, 20, 0.3f, 0.3f, 0.3f);
		
		Render.renderColor(0, length / 2, 80, length / 2, 0.3f, 0.3f, 0.3f);
		
		GL11.glPushMatrix();
		GL11.glTranslatef(-BUILDINGWIDTH, 0, 0);
		
		int c = 0;
		for(WindowChunk wc : windowChunks) {
			GL11.glPushMatrix();
			GL11.glTranslatef(0, c * CHUNKHEIGHT, 0);
			
			wc.render();
			
			GL11.glPopMatrix();
			c++;
		}
		
		GL11.glPopMatrix();
		GL11.glPopMatrix();
	}
	
	public void tick() {
		for(WindowChunk wc : windowChunks) {
			wc.tick();
		}
		
		player.tick();
		moon.tick();
		
		for(Star s : stars) {
			s.tick();
		}
		
		logoWiggler.next();
		continueStringWiggler.next();
		
		if(!gui.intro) {
			if(gui.isScrollAtWindow(-1)) {
				if(continueStringVisability < 1) {
					continueStringVisability += 0.1f;
				}
			}
			else if(continueStringVisability > 0) continueStringVisability -= 0.1f;
		}
	}
}
