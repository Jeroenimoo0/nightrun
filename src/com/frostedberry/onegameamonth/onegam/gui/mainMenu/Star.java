package com.frostedberry.onegameamonth.onegam.gui.mainMenu;

import com.frostedberry.onegameamonth.core.common.Random;
import com.frostedberry.onegameamonth.core.common.SineMove;
import com.frostedberry.onegameamonth.core.common.SineWiggler;
import com.frostedberry.onegameamonth.core.render.Render;

public class Star {
	protected Building b;
	protected float x;
	protected float y;
	protected SineWiggler sineWiggler;
	
	protected boolean scrolling;
	protected SineMove sineMove;
	protected float ma;
	protected float mb;
	
	public Star(Building b) {
		this.b = b;
		
		init();
	}
	
	public Star(Building b, float x) {
		this.b = b;
		this.x = x;
		
		init();
	}
	
	private void init() {
		sineWiggler = new SineWiggler(Random.nextInt(3) + 3, 0.05f);
		y = -150;
	}
	
	public void render() {
		Render.renderLine(x, y + (float)sineWiggler.getCurrent(), x, -600, 2, 0, 0, 0);
		Render.renderTexture("star_hd", x, y + (float)sineWiggler.getCurrent(), 32);
	}
	
	public void tick() {
		if(scrolling) {
			if(sineMove.isCompleted()) {
				y = mb;
				scrolling = false;
			}
			
			y = (float) ((mb - ma) * sineMove.getCurrentOne() + ma);
			sineMove.next();
		}
		else sineWiggler.next();
	}
	
	public void moveTo(float y) {
		if(!scrolling) {
			if(this.y != y) {
				ma = this.y;
				mb = y;
				
				if(mb < -160) mb = -160;
				
				sineMove = new SineMove(20);
				scrolling = true;
			}
		}
	}
}
