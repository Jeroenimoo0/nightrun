package com.frostedberry.onegameamonth.onegam.gui.mainMenu;

import com.frostedberry.onegameamonth.core.render.Render;

public class Window {
	protected WindowChunk wc;
	protected float x;
	protected float y;
	
	public Window(WindowChunk wc, int cx, int cy) {
		this.wc = wc;
		x = cx;
		y = cy;
	}

	public void render() {
		Render.renderTexturePixel("window", x * 40 + 20, y * 40 + 25, 40, 40, 1, 1, 1);
	}
	
	public void tick() {
		
	}
	
	public float getOnScreenY() {
		float height = wc.chunk * wc.b.CHUNKHEIGHT + y * 40 + 25 + wc.b.gui.yScroll;
		return height;
		/*
		if(height > 0) return 90 - height;
		else return 180 - (90 + height);
		//return wc.chunk * wc.b.CHUNKHEIGHT + x * 40 + 20 - wc.b.gui.yScroll;
		 */
	}
	
	public float getOnScreenX() {
		return wc.b.buildingX - wc.b.BUILDINGWIDTH + x * 40 + 20;
	}
	
	public boolean isMouseOnWindow() {
		if(isInBounds(15, wc.b.gui.getMouseOnScreenX(), getOnScreenX())) {
			if(isInBounds(19, wc.b.gui.getMouseOnScreenY(), getOnScreenY())) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isInBounds(float length, float f1, float f2) {
		return Math.abs(Math.abs(f1) - Math.abs(f2)) <= length;
	}
}
