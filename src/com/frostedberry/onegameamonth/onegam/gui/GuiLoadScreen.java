package com.frostedberry.onegameamonth.onegam.gui;

import com.frostedberry.onegameamonth.core.Core;
import com.frostedberry.onegameamonth.core.audio.AudioLoadList;
import com.frostedberry.onegameamonth.core.audio.AudioLoadObject;
import com.frostedberry.onegameamonth.core.gui.Gui;
import com.frostedberry.onegameamonth.core.gui.GuiLoadScreenTemplate;
import com.frostedberry.onegameamonth.core.texture.TextureLoadList;
import com.frostedberry.onegameamonth.core.texture.TextureLoadObject;
import com.frostedberry.onegameamonth.onegam.gui.mainMenu.GuiMainMenu;

public class GuiLoadScreen extends GuiLoadScreenTemplate {

	public GuiLoadScreen(Core core) {
		super(core);
	}

	@Override
	protected TextureLoadList getTextureLoadList() {
		TextureLoadList list = new TextureLoadList();
		
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark", "player_dark_0_0", 0, 0, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark", "player_dark_0_1", 16, 0, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark", "player_dark_1_0", 32, 0, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark", "player_dark_1_1", 48, 0, 16, 32));
		
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark", "player_dark_2_0", 0, 32, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark", "player_dark_2_1", 16, 32, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark", "player_dark_3_0", 32, 32, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark", "player_dark_3_1", 48, 32, 16, 32));
		
		list.addTextureLoadObject(new TextureLoadObject("player/skateboard", "skateboard_0", 0, 0, 32, 64));
		list.addTextureLoadObject(new TextureLoadObject("player/skateboard", "skateboard_1", 32, 0, 32, 64));
		
		list.addTextureLoadObject(new TextureLoadObject("player/player_skateboard", "player_skateboard_0", 0, 0, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_skateboard", "player_skateboard_1", 16, 0, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_skateboard", "player_skateboard_2", 32, 0, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_skateboard", "player_skateboard_3", 48, 0, 16, 32));
		
		list.addTextureLoadObject(new TextureLoadObject("player/player_skateboard", "player_skateboard_white_0", 0, 32, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_skateboard", "player_skateboard_white_1", 16, 32, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_skateboard", "player_skateboard_white_2", 32, 32, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_skateboard", "player_skateboard_white_3", 48, 32, 16, 32));
		
		/*
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_head", "player_dark_head_0", 0, 0, 16, 16));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_head", "player_dark_head_1", 16, 0, 16, 16));
		
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_eyes", "player_dark_eyes_0_0", 0, 0, 16, 16));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_eyes", "player_dark_eyes_0_1", 16, 0, 16, 16));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_eyes", "player_dark_eyes_0_2", 32, 0, 16, 16));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_eyes", "player_dark_eyes_0_3", 48, 0, 16, 16));
		
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_eyes", "player_dark_eyes_1_0", 0, 16, 16, 16));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_eyes", "player_dark_eyes_1_1", 16, 16, 16, 16));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_eyes", "player_dark_eyes_1_2", 32, 16, 16, 16));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_eyes", "player_dark_eyes_1_3", 48, 16, 16, 16));
		
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_dance", "player_dark_dance_0_0", 0, 0, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_dance", "player_dark_dance_0_1", 16, 0, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_dance", "player_dark_dance_0_2", 32, 0, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_dance", "player_dark_dance_0_3", 48, 0, 16, 32));

		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_dance", "player_dark_dance_1_0", 0, 32, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_dance", "player_dark_dance_1_1", 16, 32, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_dance", "player_dark_dance_1_2", 32, 32, 16, 32));
		list.addTextureLoadObject(new TextureLoadObject("player/player_dark_dance", "player_dark_dance_1_3", 48, 32, 16, 32));
		*/
		
		list.addTextureLoadObject(new TextureLoadObject("game_logo", "game_logo"));
		list.addTextureLoadObject(new TextureLoadObject("screen_overlay", "screen_overlay"));
		
		list.addTextureLoadObject(new TextureLoadObject("world/pole", "pole"));
		list.addTextureLoadObject(new TextureLoadObject("world/pole_blur", "pole_blur"));
		list.addTextureLoadObject(new TextureLoadObject("world/star", "star"));
		list.addTextureLoadObject(new TextureLoadObject("world/star_hd", "star_hd"));
		list.addTextureLoadObject(new TextureLoadObject("world/moon", "moon"));
		list.addTextureLoadObject(new TextureLoadObject("world/window", "window"));
		list.addTextureLoadObject(new TextureLoadObject("world/light", "light"));
		list.addTextureLoadObject(new TextureLoadObject("world/light_source", "light_source"));
		
		list.addTextureLoadObject(new TextureLoadObject("gui/key", "key_up", 0, 0, 16, 16));
		list.addTextureLoadObject(new TextureLoadObject("gui/key", "key_down", 16, 0, 16, 16));
		
		list.addTextureLoadObject(new TextureLoadObject("gui/arrows", "font_up", 0, 0, 16, 16));
		list.addTextureLoadObject(new TextureLoadObject("gui/arrows", "font_down", 16, 0, 16, 16));
		list.addTextureLoadObject(new TextureLoadObject("gui/arrows", "font_left", 32, 0, 16, 16));
		list.addTextureLoadObject(new TextureLoadObject("gui/arrows", "font_right", 48, 0, 16, 16));
		
		list.addTextureLoadObject(new TextureLoadObject("gui/checkbox", "checkbox_false", 0, 0, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("gui/checkbox", "checkbox_true", 8, 0, 8, 8));
		
		list.addTextureLoadObject(new TextureLoadObject("gui/coin", "coin", 0, 0, 16, 16));
		
		list.addTextureLoadObject(new TextureLoadObject("gui/speedicons", "speedicon_0", 0, 0, 16, 16));
		list.addTextureLoadObject(new TextureLoadObject("gui/speedicons", "speedicon_1", 16, 0, 16, 16));
		list.addTextureLoadObject(new TextureLoadObject("gui/speedicons", "speedicon_2", 0, 16, 16, 16));
		list.addTextureLoadObject(new TextureLoadObject("gui/speedicons", "speedicon_3", 16, 16, 16, 16));
		return list;
	}
	
	@Override
	protected AudioLoadList getAudioLoadList() {
		AudioLoadList all = new AudioLoadList();
		
		all.add(new AudioLoadObject("audio/soundtrack", "soundtrack"));
		all.add(new AudioLoadObject("audio/click", "click"));
		all.add(new AudioLoadObject("audio/back", "back"));
		all.add(new AudioLoadObject("audio/next", "next"));
		
		return all;
	}

	@Override
	public Gui getNextGui() {
		return new GuiMainMenu(core);
	}

}
