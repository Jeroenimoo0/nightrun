package com.frostedberry.onegameamonth.onegam.gui.intro;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;

import com.frostedberry.onegameamonth.core.Core;
import com.frostedberry.onegameamonth.core.audio.AudioPlayer;
import com.frostedberry.onegameamonth.core.gui.Gui;
import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.tick.Tick;
import com.frostedberry.onegameamonth.onegam.gui.GuiInGame;

public class GuiGameIntro extends Gui {
	protected List<RenderObject> renderObjects;
	protected List<Integer> removeRenderObjects;
	protected List<RenderObject> addRenderObjects;
	protected float backgroundColor = 0;
	
	protected boolean fading;
	protected float fa;
	protected float fb;
	protected int counter;
	
	protected float skipStringVisability;
	protected float skipStringYo;
	protected int ticks;
	
	protected int waitTicks;
	
	protected float fadeMusicSteps;
	
	public GuiGameIntro(Core core) {
		super("GuiGameIntro", core);
		
		renderObjects = new ArrayList<>();
		removeRenderObjects = new ArrayList<>();
		addRenderObjects = new ArrayList<>();
		
		renderObjects.add(new CreatedByRenderObject(this));
		
		skipStringVisability = 0;
		skipStringYo = 16;
		
		fadeMusicSteps = AudioPlayer.getAudioVolume("soundtrack") / 60f;
	}

	@Override
	public Gui getNextGui() {
		return null;
	}

	@Override
	public void render() {
		Render.renderColor(160, 90, 160, 90, backgroundColor, backgroundColor, backgroundColor);
		
		if(waitTicks > 30) {
			for(RenderObject ro : renderObjects) {
				ro.render();
			}
			
			Render.renderStringCentered("Press S to skip", 160, 172 + skipStringYo, 8, 1, 1, 1, skipStringVisability);
		}
	}

	@Override
	public void tick(Tick tick) {
		if(waitTicks > 30) {
			if(fading) {
				backgroundColor = (fb - fa) * ((float)counter / 10f) + fa;
				counter++;
				if(counter > 10) {
					fading = false;
					backgroundColor = fb;
					counter = 0;
				}
			}
			
			for(RenderObject ro : renderObjects) {
				ro.tick(tick);
				ro.tickTicks();
			}
			
			for(int i : removeRenderObjects) {
				renderObjects.remove(i);
			}
			removeRenderObjects.clear();
			
			for(RenderObject ro : addRenderObjects) {
				ro.tick(tick);
				ro.tickTicks();
				renderObjects.add(ro);
			}
			addRenderObjects.clear();
			
			if(ticks > 240) {
				if(skipStringYo < 16) skipStringYo++;
				if(skipStringVisability > 0) skipStringVisability -= 0.1f;
	 		}
			else {
				if(skipStringYo > 0) skipStringYo--;
				if(skipStringVisability < 1) skipStringVisability += 0.1f;
			}
			ticks++;
		}
		else waitTicks++;
		
		if(Keyboard.isKeyDown(Keyboard.KEY_S)) setGui(new GuiInGame(core));
		
		if(AudioPlayer.getAudioVolume("soundtrack") > 0) {
			float volume = AudioPlayer.getAudioVolume("soundtrack") - fadeMusicSteps;
			
			if(AudioPlayer.getAudioVolume("soundtrack") <= 0.1f) {
				volume = AudioPlayer.getAudioVolume("soundtrack") - fadeMusicSteps / 3;
			}
			
			if(volume <= 0) {
				volume = 0;
				AudioPlayer.stop("soundtrack");
			}
			else AudioPlayer.changeVolume("soundtrack", volume);
		}
	}
	
	public void fadeBackgroundColor(float color) {
		fading = true;
		fa = backgroundColor;
		fb = color;
		counter = 0;
	}

	public void removeRenderObject(RenderObject renderObject) {
		for(int c = 0; c < renderObjects.size(); c++) {
			if(renderObject == renderObjects.get(c)) removeRenderObjects.add(c);
		}
	}
}
