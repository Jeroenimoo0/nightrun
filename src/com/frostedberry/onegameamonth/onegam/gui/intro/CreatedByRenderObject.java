package com.frostedberry.onegameamonth.onegam.gui.intro;

import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.tick.Tick;

public class CreatedByRenderObject extends RenderObject {
	protected float color;
	protected String text;
	
	public CreatedByRenderObject(GuiGameIntro gui) {
		super(gui);
		text = "Created by Jeroen van de Haterd";
	}

	@Override
	public void tick(Tick tick) {
		if(ticks <= 440) {
			if(color < 1) color += 0.01f;
		}
		else {
			if(color > 0) color -= 0.01f;
		}
		
		if(ticks > 180) text = "aka Jeroenimoo0";
		if(ticks > 360) text = "Soundtrack by Mike Leisz @ filipinomike";
		
		if(ticks > 440 && color <= 0) {
			remove();
			add(new TalkingHeadRenderObject(gui));
		}
	}

	@Override
	public void render() {
		Render.renderStringCentered(text, 160, 90, 8, color, color, color);
	}
}
