package com.frostedberry.onegameamonth.onegam.gui.intro;

import com.frostedberry.onegameamonth.core.tick.Tick;

public abstract class RenderObject {
	protected GuiGameIntro gui;
	protected int ticks;
	
	public RenderObject(GuiGameIntro gui) {
		this.gui = gui;
	}
	
	abstract public void tick(Tick tick);
	abstract public void render();
	
	public void tickTicks() {
		ticks++;
	}
	
	public void addRenderObject(RenderObject ro) {
		gui.renderObjects.add(ro);
	}
	
	public void remove() {
		gui.removeRenderObject(this);
	}

	public void add(RenderObject ro) {
		gui.addRenderObjects.add(ro);
	}
}
