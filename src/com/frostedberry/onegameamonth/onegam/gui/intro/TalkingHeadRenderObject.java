package com.frostedberry.onegameamonth.onegam.gui.intro;

import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.tick.Tick;
import com.frostedberry.onegameamonth.onegam.gui.GuiInGame;

public class TalkingHeadRenderObject extends RenderObject {
	protected float xo;
	protected float visability;
	
	protected float overlayVisability;
	protected float finalOverlayVisability;
	
	protected float skateVisability;
	protected int skateTicks;
	
	protected String iGuess;
	
	public TalkingHeadRenderObject(GuiGameIntro gui) {
		super(gui);
		
		gui.fadeBackgroundColor(0.3f);
		
		visability = 1;
		iGuess = "I guess";
		overlayVisability = 0;
		skateVisability = 1;
		finalOverlayVisability = 0;
	}

	@Override
	public void tick(Tick tick) {
		xo += 0.05f;
		
		if(overlayVisability < 1) overlayVisability += 0.005f;
		
		if(ticks > 660 + 15 * 3 + 20) {
			if(skateTicks > 1) {
				skateTicks = 0;
				if(skateVisability == 0) skateVisability = 1;
				else skateVisability = 0;
			}
			else skateTicks++;
		}
		
		if(ticks > 600) {
			iGuess = "I guess...";
		}
		else if(ticks > 560) {
			iGuess = "I guess..";
		}
		else if(ticks > 520) {
			iGuess = "I guess.";
		}
		
		if(ticks > 630) {
			if(visability > 0) visability -= 0.1f;
		}
		
		if(ticks > 760) {
			if(finalOverlayVisability < 1) finalOverlayVisability += 0.1f;
			if(finalOverlayVisability >= 1 && ticks > 800) gui.setGui(new GuiInGame(gui.core));
		}
	}

	@Override
	public void render() {
		Render.renderTexturePixel("player_dark_0_0", 90 + xo, 200, 256, 512, 0.5f, 0.5f, 0.5f);
		
		Render.renderTexture("screen_overlay", 160, 90, 0, 256, 128, 1, 1, 1, overlayVisability);
		
		if(ticks > 120) Render.renderStringCentered("Why am i alone?", 230, 40, 8, 1, 1, 1, visability);
		if(ticks > 240) Render.renderStringCentered("What are these walls?", 230, 70, 8, 1, 1, 1, visability);
		if(ticks > 360) Render.renderStringCentered("Why is there no highscore?", 230, 100, 8, 1, 1, 1, visability);
		if(ticks > 480) Render.renderStringCentered(iGuess, 230, 130, 8, 1, 1, 1, visability);
		
		float start = 660;
		float step = 15;
		if(ticks > start && ticks < start + step) Render.renderStringCentered("I'll", 160, 60, 64, 1, 1, 1, 1);
		else if(ticks > start + step && ticks < start + step * 2) Render.renderStringCentered("Have", 160, 60, 64, 1, 1, 1, 1);
		else if(ticks > start + step * 2 && ticks < start + step * 3) Render.renderStringCentered("To", 160, 60, 64, 1, 1, 1, 1);
		else if(ticks > start + step * 3) Render.renderStringCentered("Skate", 160, 60, 64, 1, 1, 1, skateVisability);
	
		Render.renderColor(160, 90, 0, 160, 90, 0, 0, 0, finalOverlayVisability);
	}
}
